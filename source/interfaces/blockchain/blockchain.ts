/* Named aliases for basic types */
export type Satoshis = number;
export type TransactionHex = string;
export type TransactionHash = string;
export type BlockHeight = number;
export type BlockHeaderHash = string;
export type BlockHeaderHex = string;
export type Address = string;
export type ScriptHex = string;
export type ScriptHash = string;
export type DoubleSpendProofHash = string;
export type DoubleSpendProofHex = string;
export type OutputIndex = number;

/* Merkle proofs */
export type BlockMerkleRoot = string;
export type BlockMerkleProof = BlockHeaderHash[];
export type BlockMerklePosition = number;
export type TransactionMerkleRoot = string;
export type TransactionMerkleProof = TransactionHash[];
export type TransactionMerklePosition = number;

/* ... */
export interface BlockHeaderBytes
{

}

export interface BlockHeaderObject
{
	version:           number;
	previousBlockHash: BlockHeaderHash;
	merkleRoot:        BlockMerkleRoot;
	timestamp:         number;
	compressedTarget:  Uint8Array;
	nonce:             any;
}

export interface Transaction
{
	// The transaction data...
	bytes: Uint8Array;

	// If the transaction has been confirmed in a block, this refers to the height in the chain.
	blockHeight?: BlockHeight;

	// If the transaction has been confirmed in a block, this refers to the block hash..
	blockHash?: Uint8Array;

	// The merkle branches ....
	merkleProof?: TransactionMerkleProof;
}
