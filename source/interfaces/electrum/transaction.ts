/**
 * This file documents externally defined 3rd party interfaces,
 * so we need to disable some linting rules that cannot be enforced.
 *
 * Furthermore, the descriptions of theses interfaces are copied almost
 * verbatim from https://electrum-cash-protocol.readthedocs.io/en/latest/
 */

/* eslint-disable camelcase */

import type { RPCBase } from '@electrum-cash/network';

import type
{
	BlockHeight,
	TransactionMerkleProof,
	TransactionMerklePosition,
	TransactionHash,
	TransactionHex,
	OutputIndex,
	DoubleSpendProofHash,
	DoubleSpendProofHex,
} from '../blockchain';

import type
{
	TransactionStatus,
} from './protocol';

/**
 * Broadcast a transaction to the network.
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionBroadcastRequest =
{
	/**  Must be: 'blockchain.transaction.broadcast'. */
	method: 'blockchain.transaction.broadcast';

	/** The raw transaction as a hexadecimal string. */
	raw_tx: TransactionHex;
};

/**
 * The transaction hash as a hexadecimal string.
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionBroadcastResponse = TransactionHash;

/**
 * Returns information on a double-spend proof. The query can be by either tx_hash or dspid.
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionDoublespendProofGetRequest =
{
	/**  Must be: 'blockchain.transaction.dsproof.get'. */
	method: 'blockchain.transaction.dsproof.get';

	/** The transaction hash (or dspid) as a hexadecimal string. */
	hash: TransactionHash | DoubleSpendProofHash;
};

/**
 * If the transaction in question has an associated dsproof, then a JSON object. Otherwise null.
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionDoublespendProofEntryGet =
{
	dspid: DoubleSpendProofHash;
	txid: TransactionHash;
	hex: DoubleSpendProofHex;
	outpoint:
	{
		txid: TransactionHash;
		vout: OutputIndex;
	};
	descendants: TransactionHash[];
};
export type TransactionDoublespendProofGetResponse = null | TransactionDoublespendProofEntryGet;

/**
 * List all of the transactions that currently have double-spend proofs associated with them.
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionListDsProofsRequest =
{
	/**  Must be: 'blockchain.transaction.dsproof.list'. */
	method: 'blockchain.transaction.dsproof.list';
};

/**
 * A JSON array of hexadecimal strings. May be empty.
 * Each string is a transaction hash of an in-mempool transaction that has a double-spend proof associated with it.
 * Each of the hashes appearing in the list may be given as an argument to blockchain.transaction.dsproof.get() in order to obtain the associated double-spend proof for that transaction.
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionListDsProofsResponse = TransactionHash[];

/**
 * Subscribe for double spend proof notifications for a transaction.
 *
 * @memberof Blockchain.Block
 */
export type TransactionDsProofSubscribeRequest =
{
	/**  Must be: 'blockchain.transaction.dsproof.subscribe'. */
	method: 'blockchain.transaction.dsproof.subscribe';

	/** The transaction hash as a hexadecimal string. */
	tx_hash: TransactionHash;
};

/**
 * If the transaction in question has an associated dsproof, then a JSON object. Otherwise null.
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionDoublespendProofSubscribeResponse = TransactionDoublespendProofGetResponse;

/**
 * Notification for blockchain.transaction.dsproof.subscribe
 *
 * @event blockchain.transaction.dsproof.subscribe
 * @memberof Blockchain.Transaction
 */
export interface TransactionDsProofNotification extends RPCBase
{
	method: string;
	params: [ TransactionDoublespendProofGetResponse ];
}

/**
 * Unsubscribe from receiving any further dsproof notifications for a transaction.
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionDoublespendProofUnsubscribeRequest =
{
	/**  Must be: 'blockchain.transaction.dsproof.unsubscribe'. */
	method: 'blockchain.transaction.dsproof.unsubscribe';

	/** The transaction hash as a hexadecimal string. */
	tx_hash: TransactionHash;
};

/**
 * Returns true if the transaction was previously subscribed-to for dsproof notifications, otherwise false.
 * Note that false might be returned even for something subscribed-to earlier, because the server can drop subscriptions in rare circumstances.
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionDsProofUnsubscribeResponse = boolean;

/**
 * Return a raw transaction.
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionGetRequest =
{
	/**  Must be: 'blockchain.transaction.get'. */
	method: 'blockchain.transaction.get';

	/** The transaction hash as a hexadecimal string. */
	tx_hash: TransactionHash;

	/** Whether a verbose coin-specific response is required. */
	verbose?: boolean;
};

/**
 * blockchain.transaction.get
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionGetVerboseResponse = any;
export type TransactionGetResponse = TransactionHex | TransactionGetVerboseResponse;

/**
 * Returns the block height for a confirmed transaction, or 0 for a mempool transaction, given its hash.
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionGetHeightRequest =
{
	/**  Must be: 'blockchain.transaction.get_height'. */
	method: 'blockchain.transaction.get_height';

	/** The transaction hash as a hexadecimal string. */
	tx_hash: TransactionHash;
};

/**
 * blockchain.transaction.get_height
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionGetHeightUnknown = null;
export type TransactionGetHeightUnconfirmed = 0;
export type TransactionGetHeightResponse = BlockHeight | TransactionGetHeightUnconfirmed | TransactionGetHeightUnknown;

/**
 * Return the merkle branch to a confirmed transaction given its hash and height.
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionGetMerkleRequest =
{
	/**  Must be: 'blockchain.transaction.get_merkle'. */
	method: 'blockchain.transaction.get_merkle';

	/** The transaction hash as a hexadecimal string. */
	tx_hash: TransactionHash;

	/** The height at which it was confirmed, an integer. */
	height: BlockHeight;
};

/**
 * blockchain.transaction.get_merkle
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionGetMerkleResponse =
{
	merkle: TransactionMerkleProof;
	block_height: BlockHeight;
	pos: TransactionMerklePosition;
};

/**
 * Return a transaction hash and optionally a merkle proof, given a block height and a position in the block.
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionIdFromPosRequest =
{
	/**  Must be: 'blockchain.transaction.id_from_pos'. */
	method: 'blockchain.transaction.id_from_pos';

	/** The main chain block height, a non-negative integer. */
	height: BlockHeight;

	/** A zero-based index of the transaction in the given block, an integer. */
	tx_pos: TransactionMerklePosition;

	/** Whether a merkle proof should also be returned, a boolean. */
	merkle?: boolean;
};

/**
 * Transaction hash, or a dictionary with the transaction hash and merkle proof if requested.
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionIdFromPosWithProofResponse =
{
	tx_hash: TransactionHash;
	merkle: TransactionMerkleProof;
};
export type TransactionIdFromPosResponse = TransactionHash | TransactionIdFromPosWithProofResponse;

/**
 * Subscribe to a transaction in order to receive future notifications if its confirmation status changes.
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionSubscribeRequest =
{
	/**  Must be: 'blockchain.transaction.subscribe'. */
	method: 'blockchain.transaction.subscribe';

	/** The transaction hash as a hexadecimal string. */
	tx_hash: TransactionHash;
};

/**
 * blockchain.transaction.subscribe
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionSubscribeResponse = TransactionGetHeightResponse;

/**
 * Notification for blockchain.transaction.subscribe
 *
 * @event blockchain.transaction.subscribe
 * @memberof Blockchain.Transaction
 */
export interface TransactionSubscribeNotification extends RPCBase
{
	method: string;
	params: [ TransactionHash, TransactionStatus ];
}

/**
 * Unsubscribe from a transaction, preventing future notifications if its confirmation status changes.
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionUnsubscribeRequest =
{
	/**  Must be: 'blockchain.transaction.unsubscribe'. */
	method: 'blockchain.transaction.unsubscribe';

	/** The transaction hash as a hexadecimal string. */
	tx_hash: TransactionHash;
};

/**
 * blockchain.transaction.unsubscribe
 *
 * @memberof Blockchain.Transaction
 */
export type TransactionUnsubscribeResponse = boolean;
