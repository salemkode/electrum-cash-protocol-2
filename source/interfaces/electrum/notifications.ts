/**
 * This file documents externally defined 3rd party interfaces,
 * so we need to disable some linting rules that cannot be enforced.
 *
 * Furthermore, the descriptions of theses interfaces are copied almost
 * verbatim from https://electrum-cash-protocol.readthedocs.io/en/latest/
 */

/* eslint-disable camelcase */

import type { HeadersSubscribeNotification } from './header';
import type { AddressSubscribeNotification } from './address';
import type { TransactionSubscribeNotification, TransactionDsProofNotification } from './transaction';

// An RPC message limited to only valid electrum parameters.
export type ElectrumNotification = AddressSubscribeNotification | TransactionSubscribeNotification | TransactionDsProofNotification | HeadersSubscribeNotification;
