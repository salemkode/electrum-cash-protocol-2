/**
 * This file documents externally defined 3rd party interfaces,
 * so we need to disable some linting rules that cannot be enforced.
 *
 * Furthermore, the descriptions of theses interfaces are copied almost
 * verbatim from https://electrum-cash-protocol.readthedocs.io/en/latest/
 */

/* eslint-disable camelcase */

import type { RPCBase } from '@electrum-cash/network';

/* Import named types */
import type
{
	AddressStatus,
	TokenFilter,
} from './protocol';

import type
{
	Address,
	ScriptHash,
	Satoshis,
	TransactionHash,
	TransactionMerklePosition,
	BlockHeight,
} from '../blockchain';

/**
 * Request the confirmed and unconfirmed balances of a Bitcoin Cash address
 * @todo RECONSIDER THIS STRUCTURE FOR INTERFACE DOCUMENTATION.
 */
export type AddressGetBalance =
{
	/** @interface */
	Request:
	{
		/** the method name according to the electrum protocol specification. */
		method: 'blockchain.address.get_balance';

		/** the address to get balance for.  */
		address: Address;

		/** ... */
		token_filter: TokenFilter;
	};

	/** @interface */
	Response:
	{
		/** number of satoshis in UTXOs that have been confirmed in a block. */
		confirmed: Satoshis;

		/** number of satoshis in UTXOs that are still in the mempool. */
		unconfirmed: Satoshis;
	};
};

/**
 * Return the confirmed and unconfirmed balances of a Bitcoin Cash address.
 *
 * @property method {string}   - Must be: 'blockchain.address.get_balance'
 * @property address {Address} - The address as a Cash Address string (with or without prefix, case insensitive).
*/
export interface AddressGetBalanceRequest
{
	method: 'blockchain.address.get_balance';
	address: Address;
	token_filter: TokenFilter;
}

/**
 * A dictionary with keys confirmed and unconfirmed. The value of each is the appropriate balance in satoshis.
 *
 * @property confirmed {Satoshis}   - number of satoshis in UTXOs that have been confirmed in a block.
 * @property unconfirmed {Satoshis} - number of satoshis in UTXOs that are still in the mempool.
 */
export interface AddressGetBalanceResponse
{
	confirmed: Satoshis;
	unconfirmed: Satoshis;
}

/**
 * Return the confirmed and unconfirmed history of a Bitcoin Cash address.
 * @property method {string}   - Must be: 'blockchain.address.get_history'.
 * @property address {Address} - The address as a Cash Address string (with or without prefix, case insensitive).
 */
export interface AddressGetHistoryRequest
{
	method: 'blockchain.address.get_history';
	address: Address;
	from_height?: BlockHeight;
	to_height?: BlockHeight | -1;
}

/**
 * A list of confirmed transactions in blockchain order, with the output of blockchain.address.get_mempool() appended to the list.
 *
 * @property tx_hash {TransactionHash} - transaction hash used to identify a transaction.
 * @property height {BlockHeight}      - block height for the block that the transaction has been included in, or 0 for unconfirmed, or -1 for unconfirmed if one or more parents are also unconfirmed.
 * @property fee? {Satoshis}           - satoshis paid as fee for the transaction if it is currently in the mempool.
 */
export interface AddressGetHistoryEntry
{
	tx_hash: TransactionHash;
	height: BlockHeight;
	fee?: Satoshis;
}

/**
 * A list of confirmed transactions in blockchain order, with the output of blockchain.address.get_mempool() appended to the list.
 * @typedef AddressGetHistoryResponse
 * @type Array<AddressGetHistoryEntry>
 */
export interface AddressGetHistoryResponse extends Array<AddressGetHistoryEntry>
{
}

/**
 * Return the unconfirmed transactions of a Bitcoin Cash address.
 * @property method {string}   - Must be: 'blockchain.address.get_mempool'
 * @property address {Address} - The address as a Cash Address string (with or without prefix, case insensitive).
 */
export interface AddressGetMempoolRequest
{
	method: 'blockchain.address.get_mempool';
	address: Address;
}

/**
 * @property tx_hash {TransactionHash} - transaction hash used to identify a transaction.
 * @property height {BlockHeight}      - 0 if all inputs are confirmed, and -1 otherwise.
 * @property fee {Satoshis}            - satoshis paid as fee for the transaction.
 */
export interface AddressGetMempoolEntry
{
	tx_hash: TransactionHash;
	height: BlockHeight;
	fee: Satoshis;
}

/**
 * A list of mempool transactions in arbitrary order.
 */
export interface AddressGetMempoolResponse extends Array<AddressGetMempoolEntry>
{
}

/**
 * Translate a Bitcoin Cash address to a script hash. This method is potentially useful for clients preferring to work with script hashes but lacking the local libraries necessary to generate them.
 *
 * @property method {string}   - Must be: 'blockchain.address.get_scripthash'.
 * @property address {Address} - The address as a Cash Address string (with or without prefix, case insensitive)
 */
export interface AddressGetScripthashRequest
{
	method: 'blockchain.address.get_scripthash';
	address: Address;
}

/**
 * The unique 32-byte hex-encoded script hash that corresponds to the decoded address.
 */
export type AddressGetScripthashResponse = ScriptHash;

/**
 * Return an ordered list of UTXOs sent to a Bitcoin Cash address.
 * @property method {string}   - Must be: 'blockchain.address.list_unspent'.
 * @property address {Address} - The address as a Cash Address string (with or without prefix, case insensitive).
 */
export interface AddressListUnspentRequest
{
	method: 'blockchain.address.list_unspent';
	address: Address;
}

/**
 * A list of unspent outputs in blockchain order. This function takes the mempool into account. Mempool transactions paying to the address are included at the end of the list in an undefined order. Any output that is spent in the mempool does not appear.
 * @property tx_pos {TransactionMerklePosition} - TODO: Document me.
 * @property tx_hash {TransactionHash}          - TODO: Document me.
 * @property height {BlockHeight}               - TODO: Document me.
 * @property value {Satoshis}                   - TODO: Document me.
 */
export interface AddressListUnspentEntry
{
	tx_pos: TransactionMerklePosition;
	tx_hash: TransactionHash;
	height: BlockHeight;
	value: Satoshis;
}

/**
 *
 */
export interface AddressListUnspentResponse extends Array<AddressListUnspentEntry>
{
}

/**
 * Subscribe to a Bitcoin Cash address.
 * @property method {string} - Must be: 'blockchain.address.subscribe'.
 * @property address {Address} - The address as a Cash Address string (with or without prefix, case insensitive).
 */
export interface AddressSubscribeRequest
{
	method: 'blockchain.address.subscribe';
	address: Address;
}

/**
 * The status of the address.
 */
export type AddressSubscribeResponse = AddressStatus;

/**
 * Notification for blockchain.address.subscribe
 *
 * @event blockchain.address.subscribe
 */
export interface AddressSubscribeNotification extends RPCBase
{
	method: 'blockchain.address.subscribe';
	params: [ Address, AddressStatus ];
}

/**
 * Unsubscribe from a Bitcoin Cash address, preventing future notifications if its status changes.
 * @property method {string}   - Must be: 'blockchain.address.unsubscribe'.
 * @property address {Address} - The address as a Cash Address string (with or without prefix, case insensitive).
 */
export interface AddressUnsubscribeRequest
{
	method: 'blockchain.address.unsubscribe';
	address: Address;
}

/**
 * Returns True if the address was subscribed to, otherwise False. Note that False might be returned even for something subscribed to earlier, because the server can drop subscriptions in rare circumstances.
 */
export type AddressUnsubscribeResponse = boolean;
