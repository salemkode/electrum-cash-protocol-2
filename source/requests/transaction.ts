// Import typing for electrum clients.
import type { ElectrumClient } from '@electrum-cash/network';

// ...
import type
{
	// Primitives
	TransactionHex,
	TransactionHash,
	BlockHeight,

	// Transaction related requests.
	TransactionBroadcastResponse,
	TransactionGetResponse,
	TransactionGetMerkleResponse,
	TransactionGetHeightResponse,
	TransactionDoublespendProofGetResponse,
} from '../interfaces';

/**
 * @module Transactions
 * @memberof Network
 */

/**
 * TODO: (later/never?)
 * blockchain.transaction.id_from_pos
 * blockchain.transaction.dsproof.list
 */

/**
 * Broadcasts a raw transaction to the network.
 * @group Requests
 *
 * @param electrumClient - an Electrum Client used to connect to the network.
 * @param transactionHex - the raw transaction to broadcast, as a hex-encoded string.
 *
 * @returns the transactionHash of the broadcasted transaction.
 */
export async function broadcastTransaction(electrumClient: ElectrumClient, transactionHex: TransactionHex): Promise<TransactionHash>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Fetch the transaction from the network.
	const transactionHash = await electrumClient.request('blockchain.transaction.broadcast', transactionHex) as TransactionBroadcastResponse | Error;

	// Throw an error if the request failed.
	if(transactionHash instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = transactionHash.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to broadcast transaction '${transactionHex}': ${errorMessage}`));
	}

	return transactionHash;
}

/**
 * Fetches a transaction from the network.
 * @group Requests
 *
 * @param electrumClient  - an Electrum Client used to connect to the network.
 * @param transactionHash - hash of the transaction to fetch, as a hex-encoded string.
 *
 * @returns the transaction as a hex-encoded string.
 */
export async function fetchTransaction(electrumClient: ElectrumClient, transactionHash: TransactionHash): Promise<TransactionHex>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Fetch the transaction from the network.
	const transactionHex = await electrumClient.request('blockchain.transaction.get', transactionHash) as TransactionGetResponse | Error;

	// Throw an error if the request failed.
	if(transactionHex instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = transactionHex.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch transaction '${transactionHash}': ${errorMessage}`));
	}

	return transactionHex;
}

/**
 * Fetches the block height that a given transaction was included in, or a number indicating it is present in the mempool
 * @group Requests
 *
 * @param electrumClient  - an Electrum Client used to connect to the network.
 * @param transactionHash - hash of the transaction to fetch a block height for.
 *
 * @returns the block height the transaction was included in, if available.
 */
export async function fetchTransactionBlockHeight(electrumClient: ElectrumClient, transactionHash: TransactionHash): Promise<BlockHeight>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Fetch the block height of the transaction from the backend network servers.
	const blockHeight = await electrumClient.request('blockchain.transaction.get_height', transactionHash) as TransactionGetHeightResponse | Error;

	// Throw an error if the request failed.
	if(blockHeight instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = blockHeight.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch transaction confirmation height: ${errorMessage}`));
	}

	// Return the block height the transaction was included in, or it's presence in the mempool.
	return blockHeight;
}

/**
 * Fetches the transaction's merkle proof from the network.
 * @group Requests
 *
 * @param electrumClient  - an Electrum Client used to connect to the network.
 * @param transactionHash - hex-encoded string of the transaction hash
 * @param blockHeight     - number for the blockheight that transaction was stored in
 *
 * @throws if the network returns an invalid merkle proof.
 * @returns the merkle proof for the transaction, or undefined if transaction is still unconfirmed.
 */
export async function fetchTransactionProof(electrumClient: ElectrumClient, transactionHash: TransactionHash, blockHeight: BlockHeight): Promise<TransactionGetMerkleResponse | undefined>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Fetch the proof from the backend network servers.
	const transactionInclusionProof = await electrumClient.request('blockchain.transaction.get_merkle', transactionHash, blockHeight) as TransactionGetMerkleResponse | Error;

	// Throw an error if the request failed.
	if(transactionInclusionProof instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = transactionInclusionProof.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch transaction inclusion proof: ${errorMessage}`));
	}

	return transactionInclusionProof;
}

/**
 * TODO: Document me.
 * @group Requests
 *
 * @param electrumClient - an Electrum Client used to connect to the network.
 *
 * @note before calling a subscription related method, you should set up an event listener to handle the generated notifications.
 */
export async function subscribeToTransactionUpdates(electrumClient: ElectrumClient, transactionHash: TransactionHash): Promise<void>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Subscribe to status updates for the transaction.
	await electrumClient.subscribe('blockchain.transaction.subscribe', transactionHash);
}

/**
 * TODO: Document me.
 * @group Requests
 *
 * @param electrumClient - an Electrum Client used to connect to the network.
 */
export async function unsubscribeFromTransactionUpdates(electrumClient: ElectrumClient, transactionHash: TransactionHash): Promise<void>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Unsubscribe to status updates for the transaction.
	await electrumClient.subscribe('blockchain.transaction.unsubscribe', transactionHash);
}

/**
 * TODO: Document me.
 * @group Requests
 *
 * @param electrumClient - an Electrum Client used to connect to the network.
 */
export async function fetchDoublespendProof(electrumClient: ElectrumClient, transactionHash: TransactionHash): Promise<TransactionDoublespendProofGetResponse>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Fetch the transaction doublespend proof from the network.
	const doublespendProof = await electrumClient.request('blockchain.transaction.dsproof.get', transactionHash) as TransactionDoublespendProofGetResponse | Error;

	// Throw an error if the request failed.
	if(doublespendProof instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = doublespendProof.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch doublespend proof for '${transactionHash}': ${errorMessage}`));
	}

	return doublespendProof;
}

/**
 * TODO: Document me.
 * @group Requests
 *
 * @param electrumClient - an Electrum Client used to connect to the network.
 *
 * @note before calling a subscription related method, you should set up an event listener to handle the generated notifications.
 */
export async function subscribeToDoublespendUpdates(electrumClient: ElectrumClient, transactionHash: TransactionHash): Promise<void>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Subscribe to doublespend updates for the transaction.
	await electrumClient.subscribe('blockchain.transaction.dsproof.subscribe', transactionHash);
}

/**
 * TODO: Document me.
 * @group Requests
 *
 * @param electrumClient - an Electrum Client used to connect to the network.
 */
export async function unsubscribeFromDoublespendUpdates(electrumClient: ElectrumClient, transactionHash: TransactionHash): Promise<void>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Unsubscribe to doublespend updates for the transaction.
	await electrumClient.subscribe('blockchain.transaction.dsproof.unsubscribe', transactionHash);
}
