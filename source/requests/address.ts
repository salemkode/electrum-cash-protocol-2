// Import utility functions.
import { generateTokenFilter } from './utilities';

// Import typing for electrum clients.
import type { ElectrumClient } from '@electrum-cash/network';

// ...
import type
{
	// Primitives
	Address,
	BlockHeight,

	// Address related requests.
	AddressGetBalanceResponse,
	AddressGetHistoryResponse,
	AddressGetMempoolResponse,
	AddressListUnspentResponse,
} from '../interfaces';

/**
 * @module Address
 * @memberof Network
 */

/**
 * Fetches a balance for a given address.
 * @group Requests
 *
 * @param electrumClient  - an Electrum Client used to connect to the network.
 * @param address         - The address to fetch balance for.
 * @param includeSatoshis - If the balance should include outputs that does not have tokens on them.
 * @param includeTokens   - If the balance should include outputs that have tokens on them.
 *
 * @returns the number of confirmed and unconfirmed satoshis on the relevant outputs.
 */
export async function fetchBalance(electrumClient: ElectrumClient, address: Address, includeSatoshis: boolean = true, includeTokens: boolean = false): Promise<AddressGetBalanceResponse>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Construct the token filter to determine what balances to include.
	const tokenFilter = await generateTokenFilter(includeSatoshis, includeTokens);

	// Fetch the transaction from the network.
	const trustedAddressBalance = await electrumClient.request('blockchain.address.get_balance', address, tokenFilter) as AddressGetBalanceResponse | Error;

	// Throw an error if the request failed.
	if(trustedAddressBalance instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = trustedAddressBalance.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch trusted balance: ${errorMessage}`));
	}

	return trustedAddressBalance;
}

/**
 * Fetches the transaction history for an address.
 * @group Requests
 *
 * @param electrumClient    - an Electrum Client used to connect to the network.
 * @param address           - The address to fetch transaction history for.
 * @param fromHeight        - Limit transactions to those included in this and later blocks, if provided.
 * @param toHeightExclusive - Limit transactions to those included before this block, if provided.
 *
 * @returns the transactions that make up the on-chain history requested.
 */
export async function fetchHistory(electrumClient: ElectrumClient, address: Address, fromHeight: BlockHeight = 0, toHeightExclusive: any = -1): Promise<AddressGetHistoryResponse>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Fetch the history from the network.
	const addressHistory = await electrumClient.request('blockchain.address.get_history', address, fromHeight, toHeightExclusive) as AddressGetHistoryResponse | Error;

	// Throw an error if the request failed.
	if(addressHistory instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = addressHistory.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch address history: ${errorMessage}`));
	}

	return addressHistory;
}

/**
 * Fetches a list of transactions related to an address, that are currently unconfirmed in the mempool.
 * @group Requests
 *
 * @param electrumClient - an Electrum Client used to connect to the network.
 * @param address        - The address to fetch mempool transactions for.
 *
 * @returns the list of related transactions in the mempool.
 */
export async function fetchPendingTransactions(electrumClient: ElectrumClient, address: Address): Promise<AddressGetMempoolResponse>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Fetch the transactions from the network.
	const pendingTransactions = await electrumClient.request('blockchain.address.get_mempool', address) as AddressGetMempoolResponse | Error;

	// Throw an error if the request failed.
	if(pendingTransactions instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = pendingTransactions.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch pending transactions: ${errorMessage}`));
	}

	return pendingTransactions;
}

/**
 * Fetches a list of transaction outputs that are ready to be spent.
 * @group Requests
 *
 * @param electrumClient  - an Electrum Client used to connect to the network.
 * @param address         - The address to fetch a list of unspent transaction outputs for.
 * @param includeSatoshis - If the list should include outputs that does not have tokens on them.
 * @param includeTokens   - If the list should include outputs that have tokens on them.
 *
 * @return a list of unspent transaction outputs.
*/
export async function fetchUnspentTransactionOutputs(electrumClient: ElectrumClient, address: Address, includeSatoshis: boolean = true, includeTokens: boolean = false): Promise<AddressListUnspentResponse>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Construct the token filter to determine what outputs to include.
	const tokenFilter = await generateTokenFilter(includeSatoshis, includeTokens);

	// Fetch the list of unspent outputs from the network.
	const unspentOutputs = await electrumClient.request('blockchain.address.listunspent', address, tokenFilter) as AddressListUnspentResponse | Error;

	// Throw an error if the request failed.
	if(unspentOutputs instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = unspentOutputs.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch unspent outputs: ${errorMessage}`));
	}

	return unspentOutputs;
}

/**
 * @group Requests
 *
 * @param electrumClient  - an Electrum Client used to connect to the network.
 *
 * @note before calling a subscription related method, you should set up an event listener to handle the generated notifications.
 */
export async function subscribeToAddressUpdates(electrumClient: ElectrumClient, address: Address): Promise<void>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Subscribe to status updates for the address.
	await electrumClient.subscribe('blockchain.address.subscribe', address);
}

/**
 * @group Requests
 *
 * @param electrumClient  - an Electrum Client used to connect to the network.
 */
export async function unsubscribeFromAddressUpdates(electrumClient: ElectrumClient, address: Address): Promise<void>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Unsubscribe to status updates for the address.
	await electrumClient.subscribe('blockchain.address.unsubscribe', address);
}
