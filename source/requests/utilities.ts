/**
 * Creates a token filter according to electrum protocol requirements.
 */
export async function generateTokenFilter(includeSatoshis: boolean, includeTokens: boolean): Promise<string>
{
	// Don't include anything.
	if(!includeSatoshis && !includeTokens)
	{
		throw(new Error('Cannot create token filter that excludes both satoshis and tokens.'));
	}

	// Only include satoshis.
	if(includeSatoshis && !includeTokens)
	{
		return 'exclude_tokens';
	}

	// Only include tokens.
	if(!includeSatoshis && includeTokens)
	{
		return 'tokens_only';
	}

	// Include both satoshis and tokens.
	return 'include_tokens';
}
